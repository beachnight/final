class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.decimal :sale
      t.decimal :base
      t.decimal :markup
      t.decimal :tax
      t.decimal :total
      t.boolean :status
      t.integer :inventory_id
      t.integer :staff_id
      t.integer :customer_id

      t.timestamps null: false
    end
  end
end
