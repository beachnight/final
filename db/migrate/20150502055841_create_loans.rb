class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.integer :term
      t.decimal :price
      t.decimal :irate

      t.timestamps null: false
    end
  end
end
