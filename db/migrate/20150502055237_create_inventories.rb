class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.string :make
      t.string :model
      t.string :year
      t.string :color
      t.string :vin

      t.timestamps null: false
    end
  end
end
