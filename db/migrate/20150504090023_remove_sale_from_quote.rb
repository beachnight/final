class RemoveSaleFromQuote < ActiveRecord::Migration
  def change
    remove_column :quotes, :sale, :decimal
  end
end
