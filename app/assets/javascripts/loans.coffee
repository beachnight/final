# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
build_table = ->
  price = $("#loan_price").val()
  irate = $("#loan_irate").val()
  term =  $("#loan_term").val()
  $.get "/loans/loan",
    {price: price, irate: irate, term: term }

$ ->
  $('#loan_price, #loan_irate, #loan_term').bind 'keyup mouseup mousewheel', ->
    build_table()

  build_table()

