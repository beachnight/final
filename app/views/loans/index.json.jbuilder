json.array!(@loans) do |loan|
  json.extract! loan, :id, :term, :price, :irate
  json.url loan_url(loan, format: :json)
end
