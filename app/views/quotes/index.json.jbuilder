json.array!(@quotes) do |quote|
  json.extract! quote, :id, :sale, :base, :markup, :tax, :total, :status, :inventory_id, :staff_id, :customer_id
  json.url quote_url(quote, format: :json)
end
