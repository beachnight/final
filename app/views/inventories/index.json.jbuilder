json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :make, :model, :year, :color, :vin
  json.url inventory_url(inventory, format: :json)
end
