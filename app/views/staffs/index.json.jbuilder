json.array!(@staffs) do |staff|
  json.extract! staff, :id, :fname, :lname
  json.url staff_url(staff, format: :json)
end
