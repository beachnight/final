class Quote < ActiveRecord::Base
  belongs_to :inventory
  belongs_to :customer
  belongs_to :staff

  @@markupval=0.10
  @@tax=0.043

  def sold
    if self.status == false
      return "Available"
    else
      return "Sold"
    end
  end
  def marked
    mark = (base * @@markupval)
    return mark
  end

  def markedup
    markups = (base + marked)
    return markups
  end

  def taxtotals
    tax_total = (markedup * @@tax)
    return tax_total
  end

  def total
    final = ((markedup + taxtotals))
    return final
  end

  def self.grandrevenue
    gross=0
    Quote.all.each do |counter|
      if counter.status == true
        gross += counter.total
      end
    end
    return gross
  end

  def self.netprofit
    net=0
    Quote.all.each do |money|
      if money.status == true
        net += money.marked
      end
    end
    return net
  end

  def self.taxtotal
    tax=0
    Quote.all.each do |taxes|
      if taxes.status == true
        tax += taxes.taxtotals
      end
    end
    return tax
  end


end

