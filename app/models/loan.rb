class Loan < ActiveRecord::Base
  def self.payment
    @@payment
  end

  def self.interest_paid
    @@interest_paid
  end

  def self.principal_paid
    @@principal_paid
  end

  def amort_table
    return self.class.amort_table(self.price, self.irate, self.term)
  end

  def self.amort_table(price = 0.00, irate = 0.00, term = 0)
    Rails.logger.debug "Inputs: #{price} #{irate} #{term}"
    loan = []
    fixed = price
    (1..term*12).each do |counter|
      term_rate = irate/12
      @@payment = ((term_rate*fixed)/(1-(1+term_rate)**(-term*12)))
      loan << price - (@@payment - (price*irate))

      last = price - (@@payment - (price*term_rate))

      price = last

      @@interest_paid = (irate) * @@payment
      @@principal_paid = (@@payment-@@interest_paid)
    end
    return loan
  end

  end
